package com.example.lenovo.homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editText)
    EditText field;

    @BindView(R.id.clearText)
    Button clear;

    @BindView(R.id.printText)
    Button print;

    Stack<String> stack = new Stack<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.clearText)
    public void onClickClear(){
        stack.push(field.getText().toString());
        field.setText("");
    }

    @OnClick(R.id.printText)
    public void onClickPrint(){
        if(!stack.empty()) {
            field.setText(stack.pop());
        }else{
            field.setText("");
        }
    }
}
